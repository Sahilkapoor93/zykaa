# Generated by Django 2.1.1 on 2019-04-24 10:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('mysite', '0004_auto_20190424_0125'),
    ]

    operations = [
        migrations.CreateModel(
            name='delivery_partner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subtitle', models.CharField(choices=[('Mr.', 'Mr.'), ('Miss', 'Miss')], max_length=100)),
                ('profile_pic', models.FileField(blank=True, upload_to='profiles/%Y/%m/%d')),
                ('city', models.CharField(max_length=200, null=True)),
                ('age', models.IntegerField()),
                ('contact', models.IntegerField()),
                ('is_available', models.BooleanField(default=False)),
                ('registered_on', models.DateTimeField(auto_now_add=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.DecimalField(decimal_places=2, max_digits=20)),
                ('contact_name', models.CharField(max_length=250)),
                ('contact_number', models.CharField(max_length=250)),
                ('contact_email', models.CharField(max_length=250)),
                ('delivery_address', models.CharField(max_length=250)),
                ('txn_id', models.CharField(blank=True, max_length=250)),
                ('payment_mode', models.CharField(blank=True, max_length=100)),
                ('bank_name', models.CharField(blank=True, max_length=200)),
                ('status', models.CharField(blank=True, choices=[('Delivered', 'Delivered'), ('Pending', 'Pending')], default='Pending', max_length=200)),
                ('cart_id', models.CharField(max_length=500, null=True)),
                ('received_on', models.DateTimeField(auto_now_add=True)),
                ('changed_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('assigned_to', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='mysite.delivery_partner')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mysite.customer')),
            ],
        ),
        migrations.AlterField(
            model_name='food_item',
            name='course',
            field=models.CharField(choices=[('Starter', 'Starter'), ('Main Course', 'Main Course'), ('Desert', 'Desert')], max_length=1),
        ),
        migrations.AlterField(
            model_name='food_item',
            name='type',
            field=models.CharField(choices=[('Veg', 'Veg'), ('Non-Veg', 'Non-Veg')], max_length=100),
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='phone',
            field=models.IntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='res_type',
            field=models.CharField(choices=[('Bar', 'Bar'), ('Restaurant', 'Restaurant'), ('Cafe', 'Cafe'), ('Bakery', 'Bakery')], default='R', max_length=1),
        ),
    ]
